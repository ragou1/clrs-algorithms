import org.testng.Assert;
import org.testng.annotations.Test;

//PROBLEM 2.3-4
public class InsertionSortRecursive {
    //n - The index till which the array D is to be sorted.
    public static void sort(int n, int [] D){
        if(n == 0){
            return;
        }else {
            sort(n-1, D);
        }

        //Insert n-th item into correct position in sub array D[0 to n-1].
        int lastItem = D[n];
        int j = n-1;

        while (j >= 0 && lastItem < D[j]) {
            D[j + 1] = D[j];
            j--;
        }
        //Now final value of j == desired "insertion position” - 1.

        //Insert the lastItem at the correct position.
        D[j+1] = lastItem;
    }

    @Test
    public void testRecursiveInsertionSort(){
        int [] original = {3,1,2,5,4};
        int [] sorted = {1,2,3,4,5};

        sort(original.length-1, original);

        Assert.assertEquals(original, sorted, "Original array is not sorted");
    }

}
