import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//PROBLEM 2.3-5
public class BinarySearch {
    //Array arr is already sorted in increasing order.
    public static int search(int [] arr, int toFind){
        int start = 0;
        int end = arr.length-1;
        int mid = 0;

        while(start < end){
            mid = (start + end)/2;

            if(arr[mid] == toFind){
                return mid;
            }

            if(toFind < arr[mid]){
                end = mid-1;
            }else {
                start = mid+1;
            }
        }
        //Now start should == end.

        return (arr[start] == toFind) ? start : -1;
    }


    //Array arr is already sorted in increasing order.
    public static int searchRecursively(int [] arr, int toFind, int start, int end){
        //If this happens, then the element has not been found in the array.
        if(start > end){
            return -1;
        }

        int mid = (start + end)/2;

        if(arr[mid] == toFind){
            return mid;
        }else {
            if(toFind < arr[mid] ){
                end = mid-1;
            }else {
                start = mid+1;
            }
            mid = searchRecursively(arr, toFind, start, end);
        }
        return mid;
    }

    @DataProvider(name = "numbersToFind")
    public static Object[][] numbersToFindInArray()
    {
        int [] z = {3,7,10,15,18,27,30};
        int [] y = {21};
        int [] x = {2,3,4,4,7};

        return new Object[][] {
                //Data array format - Array to search, number to find, expected index of the number.
                {z, z[0], 0}, //number is at start of array.
                {z, z[z.length-1], z.length-1}, //number is at end of array.
                {z, z[0+1], 0+1}, //number is anywhere between start & end of array.
                //number is not in array, and is...
                {z, z[0]-1, -1},//less than min element.
                {z, z[z.length-1]+1, -1},//greater than max element.
                {z, z[0+1]+1, -1},//greater than any in-between element.
                //array has only one element.
                {y, y[0], 0},
                //Accidental feature - if number is repeated, then return index of first occurrence.
                {x, x[2], 2}
        };
    }

    @Test(description = "Find a number in an array by using binary search.", dataProvider = "numbersToFind")
    public void findNumberInArray(int [] array, int numberToFind, int expectedIndexOfNumber){
        Assert.assertEquals(search(array, numberToFind), expectedIndexOfNumber,
                "Could not find " + numberToFind + " at " + expectedIndexOfNumber);
    }

    @Test(description = "Find a number in an array by using recursive binary search.", dataProvider = "numbersToFind")
    public void findNumberInArrayUsingRecursion(int [] array, int numberToFind, int expectedIndexOfNumber){
        Assert.assertEquals(searchRecursively(array, numberToFind, 0, array.length-1), expectedIndexOfNumber,
                "Could not find " + numberToFind + " at " + expectedIndexOfNumber);
    }

}
