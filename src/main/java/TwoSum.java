import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;

//PROBLEM 2.3-7
public class TwoSum {

    //Taken from Rutger university solutions.
    //Assumptions - S is already sorted in increasing order (say, by using Merge Sort algorithm).
    //The min sum of 2 elements is S[0] + S[last] and max is S[last-1] + S[last]
    public static int [] findSum_1(int x, int [] S){
        int i = 0;
        int j = S.length-1;
        int [] foundAtIndexes = {-1, -1};
        int sum = 0;

        while(i < j){
            sum = S[i] + S[j];

            if(x == sum){
                foundAtIndexes[0] = i;
                foundAtIndexes[1] = j;
                return foundAtIndexes;
            }

            if(x < sum){
                j--;
            }else {
                i++;
            }
        }
        return foundAtIndexes;
    }


    //Taken from Koushik Kothagal's Java Brains youtube channel - https://www.youtube.com/watch?v=TcsYEnMrnFo
    /*
    * Summary - It uses a Map to find if a "two sum" exists in an array. Worse case runtime is O(n).
    * Example - Can the sum x = 10 be found in S = {20,15,3,10,7} ? Yes, 3 + 7 in S equals 10.
    * A map M will store deltas (i.e. x-S[i]) for each S[i]. M is like { (x-s[i], i) }. For each S[i],
    * we check if its delta is present in M. If present, then we can say that sum x can be found in S.
    * */
    public static int [] findSum_2(int x, int [] S){
        int [] foundAtIndexes = {-1, -1};
        Map<Integer, Integer> deltas = new HashMap<>();
        int delta = 0;

        for(int i = 0; i < S.length; i++){
            if(deltas.containsKey(S[i])){
                foundAtIndexes[0] = deltas.get(S[i]);
                foundAtIndexes[1] = i;
                return foundAtIndexes;
            }else {
                delta = x - S[i];
                if(delta < 0){ continue; }
                deltas.put(delta, i);
            }
        }
        return foundAtIndexes;
    }

    @Test(dataProvider = "sortedTestData")
    public void testSumFinder_1(int sum, int [] S, int [] expectedIndexes){
        int [] foundAtIndexes = findSum_1(sum, S);
        Assert.assertEquals(foundAtIndexes, expectedIndexes, "Did not find the sum in expected indexes !" );
    }

    @Test(dataProvider = "unsortedTestData")
    public void testSumFinder_2(int sum, int [] S, int [] expectedIndexes){
        int [] foundAtIndexes = findSum_2(sum, S);
        Assert.assertEquals(foundAtIndexes, expectedIndexes, "Did not find the sum in expected indexes !" );
    }

    @DataProvider(name = "sortedTestData")
    public Object [] [] twoSumWithSortedTestData(){
        int [] s = {3, 6, 10, 17, 20, 25, 31};//Must always be sorted in increasing order !
        int last = s.length-1;
        int mid = last/2;

        int [] s1 = {2,5,7,7,9,10};//Sum = 7 + 10 and 7 is duplicated.

        return new Object[][] {
                //Sum is present in array...
                {s[0] + s[last], s, new int [] {0, last}}, //and is smallest sum in array.
                {s[last-1] + s[last], s, new int [] {last-1, last}}, //and is largest sum in array.
                {s[mid] + s[mid-1], s, new int [] {mid-1, mid}}, //and is present in between the array.

                //Sum is not present in array.
                {1000, s, new int[] {-1, -1}},

                //Sum is present in array and one of the two numbers making up sum is duplicated in array.
                {7 + 10, s1, new int [] {2,5}}
        };
    }


    @DataProvider(name = "unsortedTestData")
    public static Object[] [] twoSumWithUnsortedData(){
        int [] s = {25, 6, 3, 20, 17, 31, 10};

        return new Object[][]{
                //Sum is present in array.
                {s[4] + s[6], s, new int [] {4, 6}},
                //Sum is not present in array.
                {1000, s, new int [] {-1, -1}}
        };
    }
}
